#ifndef __ALL_HEADERS__
#define __ALL_HEADERS__
#include "evaluation/aike.h"
#include "evaluation/alergia.h"
#include "evaluation/depth-driven.h"
#include "evaluation/evidence-driven.h"
#include "evaluation/fixed_depth_regression.h"
#include "evaluation/full-overlap-driven.h"
#include "evaluation/kldistance.h"
#include "evaluation/likelihood.h"
#include "evaluation/mealy.h"
#include "evaluation/mse-error.h"
#include "evaluation/num_count.h"
#include "evaluation/overlap-driven.h"
#endif
