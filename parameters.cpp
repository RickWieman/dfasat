#include "parameters.h"

int alphabet_size = 0;
bool MERGE_SINKS = 0;
int STATE_COUNT = 0;
int SYMBOL_COUNT = 0;
float CORRECTION = 0.0;
float CHECK_PARAMETER = 0.0;
bool USE_SINKS = 0;
float MINIMUM_SCORE = 0;
float LOWER_BOUND = 0;
int GREEDY_METHOD = 0;
int APTA_BOUND = 0;
int CLIQUE_BOUND = 0;
bool EXTEND_ANY_RED = 0;
bool MERGE_SINKS_PRESOLVE = 0;
bool MERGE_SINKS_DSOLVE = 0;
int OFFSET = 1;
int EXTRA_STATES = 0;
bool TARGET_REJECTING = 0;
bool SYMMETRY_BREAKING = 0;
bool FORCING = 0;
bool MERGE_MOST_VISITED = 0;
bool MERGE_BLUE_BLUE = 0;
bool RED_FIXED = 0;
bool MERGE_WHEN_TESTING = 0;
bool DEPTH_FIRST = 0;
string eval_string;

