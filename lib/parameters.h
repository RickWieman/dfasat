#ifndef __PARAMETERS_H__
#define __PARAMETERS_H__

#include <string>
using namespace std;

extern int alphabet_size;
extern bool MERGE_SINKS;
extern int STATE_COUNT;
extern int SYMBOL_COUNT;
extern float CORRECTION;
extern float CHECK_PARAMETER;
extern bool USE_SINKS;
extern float LOWER_BOUND;
extern int alphabet_size;
extern int GREEDY_METHOD;
extern int APTA_BOUND;
extern int CLIQUE_BOUND;
extern bool EXTEND_ANY_RED;
extern bool MERGE_SINKS_PRESOLVE;
extern int OFFSET;
extern int EXTRA_STATES;
extern bool TARGET_REJECTING;
extern bool SYMMETRY_BREAKING;
extern bool FORCING;
extern bool MERGE_SINKS_DSOLVE;
extern string eval_string;
extern bool MERGE_MOST_VISITED;
extern bool MERGE_BLUE_BLUE;
extern bool RED_FIXED;
extern bool MERGE_WHEN_TESTING;
extern bool DEPTH_FIRST;

#endif
